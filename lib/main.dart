
import 'package:firebase_app_project/aboutUs.dart';
import 'package:firebase_app_project/customers.dart';
import 'package:firebase_app_project/developer.dart';
import 'package:firebase_app_project/google_sign_in.dart';
import 'package:firebase_app_project/home.dart';
import 'package:firebase_app_project/splashPage.dart';
import 'package:firebase_app_project/splashPage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'pageRoutes.dart';

import 'homepage.dart';
import 'login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp( MyApp());
}

class MyApp extends StatelessWidget {

  //const MyApp({Key key}) : super(key: key);
  static final String title = 'MainPage';

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false,
      routes: {
        pageRoutes.customer: (context) => Customer(),

        pageRoutes.developer: (context) => Developer(),
        pageRoutes.home: (context) => Home(),
        pageRoutes.login: (context) => Login(),
        pageRoutes.about: (context) => aboutUs(),
        pageRoutes.homepage: (context) => HomePage(),

      },
      title: 'CRM System',
      home: splashPage(),
    );

  }
 /*Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => GoogleSignInProvider(),
          child: MaterialApp(
    debugShowCheckedModeBanner: false,
    title: title,
    theme: ThemeData.dark().copyWith(accentColor: Colors.indigo),
    home: Login(),
  )
  );*/
}

