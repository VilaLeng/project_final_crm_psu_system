import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget createDrawerHeader() {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill, image: AssetImage('assets/images/logo3.jpg')
          )
      ),
      child: Stack(children: <Widget>[

        Positioned(

            bottom: 12.0,
            left: 16.0,
            child: Text("CRM OF PSU SYSTEM",
                style: TextStyle(
                    color: Colors.indigo,
                    // backgroundColor: Colors.white38,
                    backgroundColor: Colors.white54,
                    fontSize: 27,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto'
                )

            )
        ),
      ]));
}
