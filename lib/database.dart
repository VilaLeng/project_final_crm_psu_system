import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:firebase_app_project/profile_model.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_profile.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE profileDB (
          id INTEGER PRIMARY KEY,
          firstname TEXT,
          lastname TEXT,
          email TEXT,
          phone TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<BookingModel>> getGroceries() async {
    Database db = await instance.database;
    var groceries = await db.query('profileDB', orderBy: 'firstname');
    List<BookingModel> groceryList = groceries.isNotEmpty
        ? groceries.map((c) => BookingModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<BookingModel>> getProfile(int id) async{
    Database db = await instance.database;
    var profile = await db.query('profileDB', where: 'id = ?', whereArgs: [id]);
    List<BookingModel> profileList = profile.isNotEmpty
        ? profile.map((c) => BookingModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(BookingModel grocery) async {
    Database db = await instance.database;
    return await db.insert('profileDB', grocery.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('profileDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(BookingModel grocery) async {
    Database db = await instance.database;
    return await db.update('profileDB', grocery.toMap(),
        where: "id = ?", whereArgs: [grocery.id]);
  }
}