

import 'dart:io';

import 'package:firebase_app_project/profile_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'database.dart';

class ShowProfile extends StatelessWidget {
  final id;

  ShowProfile({Key key,  this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Booking Detail Page',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 23,
              color: Colors.white70),),
      ),
      body: Center(
        child: FutureBuilder<List<BookingModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<BookingModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data.map((profile) {
                  return Center(
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage: FileImage(File(profile.image)),
                          radius: 145,
                        ),
                        Text(
                          'Name: ${profile.firstname}  ${profile.lastname}',
                          style: TextStyle(
                              fontSize: 21,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                        Text(
                          'Hotel: ${profile.email}',
                          style: TextStyle(
                              fontSize: 21,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                        Text(
                          'Room: ${profile.phone}',
                          style: TextStyle(
                              fontSize: 21,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                        AlertDialog(
                          title: Text('Wanna Reset Your Data?',style: TextStyle(
                              fontSize: 21,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue),),
                          content: Text(
                            'If you have found some mistake or any new data you want to add please click YES, if no'
                                'please click NO.',
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: Colors.blueGrey),
                          ),
                          actions: [
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'YES',
                                style: TextStyle(
                                    fontSize: 21,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'NO',
                                style: TextStyle(
                                    fontSize: 21,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
