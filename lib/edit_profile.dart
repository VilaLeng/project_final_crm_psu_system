
import 'dart:io';

import 'package:firebase_app_project/profile_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'database.dart';

class EditProfile extends StatefulWidget {
  BookingModel model;

  EditProfile(this.model);

  @override
  _EditProfileState createState() => _EditProfileState(model);
}

class _EditProfileState extends State<EditProfile> {
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var email = TextEditingController();
  var phone = TextEditingController();

  BookingModel model;
  int selectedId;

  var _image;
  var imagePicker;
  _EditProfileState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    firstname  = TextEditingController()..text = this.model.firstname;
    lastname = TextEditingController()..text = this.model.lastname;
    email = TextEditingController()..text = this.model.email;
    phone = TextEditingController()..text = this.model.phone;
    _image = this.model.image;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Booking Page',style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 23,
            color: Colors.white70),),
      ),
      body: Container(
        color: Colors.blueGrey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    buildTextfield("Customer Name", firstname),
                    buildTextfield("Hotel Name", lastname),
                    buildTextfield("Room Type", email),
                    buildTextfield("Staying Period", phone),
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                          BoxDecoration(color: Colors.blueAccent[200]),
                          child: _image != null
                              ? Image.file(
                            File(_image),
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.fitHeight,
                          )
                              : Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(18),
                                color: Colors.blueAccent[200]),
                            width: 200,
                            height: 200,
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.grey[800],
                            ),
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Update'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Update') {
          await DatabaseHelper.instance.update(
            BookingModel(
                id: selectedId,
                firstname: firstname.text,
                lastname: lastname.text,
                email: email.text,
                phone: phone.text,
                image: _image),
          );
          setState(() {
            //firstname.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white38,
          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(15.0)),),
        ),
      ),
    );
  }

}