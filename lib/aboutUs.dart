import 'package:firebase_app_project/customers.dart';
import 'package:firebase_app_project/datacenter.dart';
import 'package:firebase_app_project/developer.dart';
import 'package:firebase_app_project/home.dart';
import 'package:firebase_app_project/homepage.dart';
import 'package:firebase_app_project/login.dart';
import 'package:firebase_app_project/signup.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'drawerbody.dart';
import 'drawerheader.dart';

class aboutUs extends StatelessWidget {
  static const String routeName = '/about';

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      //backgroundColor: Colors.white54,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,

        title: Text(
          "CRM of PSU",
          style: TextStyle(
              color: Colors.white54, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        //leading: Icon(Icons.menu, color: Colors.white54,),
        //placing inbuilt icon on leading of appbar
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Signup()));
            },
            icon: Icon(FontAwesomeIcons.envelope, color: Colors.white54,),
            //using font awesome icon in action list of appbar
          ),
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login()));
            },
            icon: Icon(FontAwesomeIcons.facebook, color: Colors.white54,),
            //cart+ icon from FontAwesome
          ),
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login()));
            },
            icon: Icon(FontAwesomeIcons.google, color: Colors.white54,),
            //cart+ icon from FontAwesome
          ),
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Developer()));
            },
            icon: Icon(FontAwesomeIcons.userAlt, color: Colors.white54,),
            //cart+ icon from FontAwesome
          ),
        ],
      ),

      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Customer Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => HomePage())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Developer Page',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => Developer())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'Administration Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Home())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.book,
              text: 'About System Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => aboutUs())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'Login Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      ), //set app bar
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(FontAwesomeIcons.android, size: 37,),
                  iconColor: Colors.blueGrey,
                  title: const Text(
                      'Customer Relationship Management of PSU System',
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      )),
                  subtitle: Text(
                    'CRM of PSU System Details',
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'The priority task of the CRM of PSU System is making the HR and other entire fields of'
                        'the business and company feel for comfortable in managing the customers and consumers as'
                        'well as royal members of the company. The CRM of PSU System will make the company and '
                        'business communicate with customers as Family.',
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    FlatButton(
                      // textColor: const Color(0xFF051D74),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Customer()));
                        // Perform some action
                      },
                      child: const Text(
                        'Information Customers',
                        style: TextStyle(
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          backgroundColor: Colors.black26,
                        ),
                      ),
                    ),
                    FlatButton(
                      //  textColor: const Color(0xFF051D74),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Developer()));
                        // Perform some action
                      },
                      child: const Text(
                        'Information Developers',
                        style: TextStyle(
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          backgroundColor: Colors.black26,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset('assets/images/logo3.jpg'),
                SizedBox(
                  height: 16,
                ),
                Image.asset('assets/images/logo2.jpg'),
                SizedBox(height: 15),
                ElevatedButton.icon(
                  onPressed: () {
                    //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                    //
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => DataCenter()));
                  },
                  icon: Icon(
                    Icons.arrow_right,
                    color: Colors.white54, size: 37,
                  ),
                  label: Text("Go to Data Chart", style: TextStyle(
                      color: Colors.white54, fontSize: 17, fontWeight:FontWeight.bold),),
                  style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
                ),
              ],
            ),
          ),
          /* AlertDialog(
            title: Text('Reset settings?'),
            content: Text('This will reset your device to its default factory settings.'),
            actions: [
              FlatButton(
                textColor: Color(0xFF6200EE),
                onPressed: () {},
                child: Text('CANCEL'),
              ),
              FlatButton(
                textColor: Color(0xFF6200EE),
                onPressed: () {},
                child: Text('ACCEPT'),
              ),
            ],
          )*/
        ],
      ),
    );
  }
}
