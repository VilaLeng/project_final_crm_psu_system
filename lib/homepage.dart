import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_app_project/customers.dart';
import 'package:firebase_app_project/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'aboutUs.dart';
import 'authentication.dart';
import 'customers.dart';
import 'developer.dart';
import 'drawerbody.dart';
import 'drawerheader.dart';
import 'login.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);
  static const String routeName = '/HomePage';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text fields' controllers
  final TextEditingController _fullnameController = TextEditingController();
  final TextEditingController _phonenumberController = TextEditingController();
  final TextEditingController _genderController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();
  final TextEditingController _provinceController = TextEditingController();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _customers =
      FirebaseFirestore.instance.collection('customers');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _fullnameController.text = documentSnapshot['fullname'];
      _phonenumberController.text = documentSnapshot['phonenumber'];
      _genderController.text = documentSnapshot['gender'];
      _ageController.text = documentSnapshot['age'].toString();
      _provinceController.text = documentSnapshot['province'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _fullnameController,
                  decoration: const InputDecoration(labelText: 'Full Name'),
                ),
                TextField(
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  controller: _ageController,
                  decoration: const InputDecoration(
                    labelText: 'Age: ',
                  ),
                ),
                TextField(
                  controller: _phonenumberController,
                  decoration:
                      const InputDecoration(labelText: 'Phone Number: '),
                ),
                TextField(
                  controller: _genderController,
                  decoration: const InputDecoration(labelText: 'Gender: '),
                ),
                /* TextField(
                  controller: _ageController,
                  decoration: const InputDecoration(labelText: 'Age: '),
                ),*/
                TextField(
                  controller: _provinceController,
                  decoration: const InputDecoration(labelText: 'Province: '),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String fullname = _fullnameController.text;
                    final String gender = _genderController.text;
                    final String phonenumber = _phonenumberController.text;
                    final String province = _provinceController.text;
                    final double age = double.tryParse(_ageController.text);
                    if (fullname != null &&
                        phonenumber != null &&
                        gender != null &&
                        age != null &&
                        province != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _customers
                            .add({
                              "fullname": fullname,
                              "phonenumber": phonenumber,
                              'age': age,
                              'gender': gender,
                              'province': province,
                            })
                            .then((value) => print("Customer Added"))
                            .catchError((error) =>
                                print("Failed to Add Customer: $error"));
                      }

                      if (action == 'update') {
                        // Update the product
                        await _customers
                            .doc(documentSnapshot.id)
                            .update({
                              "fullname": fullname,
                              "phonenumber": phonenumber,
                              'age': age,
                              'gender': gender,
                              'province': province,
                            })
                            .then((value) => print("Customer Updated"))
                            .catchError((error) =>
                                print("Failed to Update Customer: $error"));
                      }

                      // Clear the text fields
                      _fullnameController.text = '';
                      _phonenumberController.text = '';
                      _provinceController.text = '';
                      _ageController.text = '';
                      _genderController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String customerId) async {
    await _customers
        .doc(customerId)
        .delete()
        .then((value) => print("Customer Deleted"))
        .catchError((error) => print("Failed to delete customer: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a customer')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white54,
              size: 30,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (contex) => Login()),
                      ));
            },
          )
        ],
        title: const Text(
          'Customers List Management',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 19, color: Colors.white54),
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Customer Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.home),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Customer())),
            ),
            // createDrawerHeader(),

            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Developer Page',
              onTap: () =>
                  //Navigator.pushReplacementNamed(context, pageRoutes.profile),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Developer())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'Administration Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.event),
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.book,
              text: 'About System Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.event),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => aboutUs())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'Login Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.event),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      ),
      //set app bar
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _customers.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data.docs[index];
                return Card(
                  color: Colors.white10,
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(
                      documentSnapshot['fullname'],
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    subtitle: Text(
                      documentSnapshot['phonenumber'],
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(
                                Icons.edit,
                                color: Colors.blueGrey,
                              ),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                            icon: const Icon(
                              Icons.delete,
                              color: Colors.blueGrey,
                            ),
                            /* onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)*/
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    backgroundColor: Colors.white54,
                                    title: new Text(
                                      "Do you want to delete the customer?",
                                      style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 23,
                                      ),
                                    ),
                                    // content: new Text("Please Confirm"),
                                    actions: [
                                      new TextButton(
                                        onPressed: () {
                                          _deleteProduct(documentSnapshot.id);
                                          setState(() {
                                            Navigator.of(context).pop();
                                          });
                                        },
                                        /* onPressed: () =>
                                       _deleteProduct(documentSnapshot.id),*/
                                        child: new Text("Ok",style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 23,
                                        ),),
                                      ),
                                      Visibility(
                                        visible: true,
                                        child: new TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: new Text("Cancel",style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 23,
                                          ),),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Customer()));
                      // builder: (context) => GetUserName(documentId)));
                    },
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),

      // Add new product
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueGrey,
        onPressed: () => _createOrUpdate(),
        child: const Icon(
          Icons.add,
          color: Colors.white54,
        ),
      ),
    );
  }
}
